import PropTypes from 'prop-types'
import React from 'react'

import ClickOutside from './ClickOutside'

export default class TextArea extends React.Component {
  static propTypes = {
    placeholder: PropTypes.string,
    text: PropTypes.string,
    /**
     * This could be the line height if it was a higher number than the font size.
     */
    offsetHeight: PropTypes.number,
    /**
     * In order to avoid outside popups clicks triggering edit. Event bubbling cannot be stopped because click outside
     * component uses native DOM events and the React synthetic event still bubbles.
     */
    canTrigger: PropTypes.bool,

    onSubmit: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      text: null,
      height: this.props.offsetHeight * 2,
    }
    this.textareaRef = React.createRef()
  }

  handleChange = (event) => {
    this.setState({ text: event.target.value })
    this.adjustTextareaHeight()
  }

  handleKeyPress = (event) => event.key === 'Enter' && this.handleSubmit()

  handleSubmit = () => {
    this.props.onSubmit(this.state.text)
    this.setState({ text: null, height: this.props.offsetHeight * 2 })
  }

  handleClick = (event) => {
    this.props.canTrigger && this.setState({ text: this.props.text },
      () => {
        this.adjustTextareaHeight()
        this.textareaRef.current.focus()
        this.textareaRef.current.setSelectionRange(this.state.text.length, this.state.text.length)
      })
  }

  adjustTextareaHeight = () => {
    /**
     * If area is smaller than make it bigger
     */
    let height = this.textareaRef.current.clientHeight
    while (height < this.textareaRef.current.scrollHeight) {
      height += this.props.offsetHeight
    }
    this.setState({ height })
  }

  render () {
    return (
      <div className='textarea'>
        {this.state.text !== null
          ? (
            <ClickOutside onClickOutside={this.handleSubmit}>
              <div className='input-wrapper'>
                <textarea
                  style={{ height: this.state.height }} placeholder='Add description...'
                  onKeyPress={this.handleKeyPress}
                  onChange={this.handleChange} value={this.state.text} ref={this.textareaRef}
                />
              </div>
            </ClickOutside>)
          : <div onClick={this.handleClick}>{this.props.text ? this.props.text : this.props.placeholder}</div>}
      </div>
    )
  }
}
