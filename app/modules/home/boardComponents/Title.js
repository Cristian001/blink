import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MoreOutlined } from '@ant-design/icons'

import { changeBoardTitle, moveAllCards } from '../actions'
import Popup from '../../reusable/Popup'

const mapStateToProps = (state) => ({
  boards: state.boards,
})
const mapDispatchToProps = (dispatch) => bindActionCreators({ changeBoardTitle, moveAllCards }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Title extends React.Component {
  static propTypes = {
    boardId: PropTypes.number,
    boards: PropTypes.object,

    changeBoardTitle: PropTypes.func,
    moveAllCards: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      title: null,
      popupVisible: false,
    }
    this.titleRef = React.createRef()
  }

  getThisBoard = () => this.props.boards[this.props.boardId]

  getOtherBoards = () => Object.values(this.props.boards).filter((board) => board.id !== this.props.boardId)

  /**
   * Rename board
   */
  handleClick = () => {
    this.setState({ title: this.getThisBoard().title.toUpperCase() },
      () => this.titleRef.current.focus())
  }

  handleChange = (event) => this.setState({ title: event.target.value.toUpperCase() })

  handleKeyPress = (event) => event.key === 'Enter' && this.changeBoardTitle()

  changeBoardTitle = () => {
    this.props.changeBoardTitle(this.props.boardId, this.state.title)
    this.setState({ title: null })
  }

  /**
   * Move all cards
   */
  handleMenuClick = () => this.setState({ popupVisible: true })

  handleClose = () => this.setState({ popupVisible: false })

  handleMoveAll = (boardId) => {
    this.props.moveAllCards(this.props.boardId, boardId)
    this.handleClose()
  }

  render () {
    return (
      <>
        <div className='title'>
          {this.state.title !== null
            ? (
              <div className='input-wrapper'>
                <input
                  type='text' onChange={this.handleChange} value={this.state.title}
                  onKeyPress={this.handleKeyPress} ref={this.titleRef} onBlur={() => this.changeBoardTitle()}
                />
              </div>)
            : <h2 onClick={this.handleClick}>{this.getThisBoard().title}</h2>}
          <MoreOutlined onClick={this.handleMenuClick} />
          <Popup title='Move all cards' onClose={this.handleClose} visible={this.state.popupVisible}>
            {this.getOtherBoards().map((board, idx) =>
              <div className='item' key={idx} onClick={() => this.handleMoveAll(board.id)}>
                {board.title}
              </div>)}
          </Popup>
        </div>
      </>
    )
  }
}
