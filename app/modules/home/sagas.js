import { takeEvery, select, put } from 'redux-saga/effects'

import { CREATE_CARD, ADD_NEW_CARD } from './actions'

export function * cardActionsSaga () {
  yield takeEvery(ADD_NEW_CARD, function * (action) {
    const cards = yield select((state) => state.cards)
    const newCardId = Math.max(...Object.keys(cards).map((key) => parseInt(key))) + 1
    yield put({ ...action, type: CREATE_CARD, cardId: newCardId })
  })
}
