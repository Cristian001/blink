import React from 'react'
import hoistNonReactStatics from 'hoist-non-react-statics'
import { ReactReduxContext } from 'react-redux'

import getInjectors from './sagaInjectors'

/**
 * Dynamically injects a saga, passes component's props as saga arguments
 *
 * @param {Object[]} sagas - Array of objects describing a saga
 * @param {string} sagas[].key A key of the saga
 * @param {function} sagas[].saga A root saga that will be injected
 * @param {string} sagas[].[mode] By default (constants.RESTART_ON_REMOUNT) the saga will be started on component mount
 * and cancelled with `task.cancel()` on component un-mount for improved performance. Another two options:
 *   - constants.DAEMON—starts the saga on component mount and never cancels it or starts again,
 *   - constants.ONCE_TILL_UNMOUNT—behaves like 'RESTART_ON_REMOUNT' but never runs it again.
 *
 */
export default (sagas) => (WrappedComponent) => {
  class InjectSagas extends React.Component {
    static WrappedComponent = WrappedComponent

    static contextType = ReactReduxContext

    static displayName = `withSagas(${(WrappedComponent.displayName || WrappedComponent.name || 'Component')})`

    constructor (props, context) {
      super(props, context)
      this.injectors = getInjectors(context.store)
      this.injectors.injectSagas(sagas)
    }

    componentWillUnmount () {
      this.injectors.ejectSagas(sagas.map((entry) => entry.key))
    }

    render () {
      return <WrappedComponent {...this.props} />
    }
  }

  return hoistNonReactStatics(InjectSagas, WrappedComponent)
}
