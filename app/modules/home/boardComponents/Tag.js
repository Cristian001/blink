import PropTypes from 'prop-types'
import React from 'react'

export default class Tag extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    minimized: PropTypes.bool,
    className: PropTypes.string,
    toggleMinimize: PropTypes.func,
  }

  handleClick = (event) => {
    event.stopPropagation()
    this.props.toggleMinimize()
  }

  render () {
    const className = this.props.minimized ? 'minimized' : ''
    return (
      <div className={`${this.props.className} ${className}`} onClick={this.handleClick}>
        {this.props.text}
      </div>
    )
  }
}
