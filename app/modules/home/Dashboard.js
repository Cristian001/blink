import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Board from './Board'

const mapStateToProps = (state) => ({
  boards: state.boards,
})
const mapDispatchToProps = (dispatch) => bindActionCreators({ }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Dashboard extends React.Component {
  static propTypes = {
    boards: PropTypes.object,
  }

  constructor (props) {
    /**
     * Keep track of dragging node, originating board, and current target board and pass this state to board children.
     */

    super(props)
    this.initialState = {
      draggedNode: null,
      draggedNodeId: null,
      sourceBoardId: null,
      currentTargetBoardId: null,
    }
    this.state = { ...this.initialState }
  }

  /**
   * Called when dragging starts
   */
  setup = (draggedNode, draggedNodeId, sourceBoardId, currentTargetBoardId) =>
    this.setState({ draggedNode, draggedNodeId, sourceBoardId, currentTargetBoardId })

  /**
   * Called when reaching a new board
   */
  setCurrentTargetBoardId = (currentTargetBoardId) =>
    this.setState((state) => ({ ...state, currentTargetBoardId }))

  /**
   * Called when dragging ends
   */
  reset = () => this.setState({ ...this.initialState })

  render () {
    return (
      <div className='main-area'>
        {Object.values(this.props.boards).map((board) =>
          <Board
            id={board.id} key={board.id} {...this.state} setup={this.setup}
            setCurrentTargetBoardId={this.setCurrentTargetBoardId} reset={this.reset}
          />)}
      </div>
    )
  }
}
