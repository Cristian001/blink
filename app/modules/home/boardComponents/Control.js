import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { addNewCard } from '../actions'
import ClickOutside from '../../reusable/ClickOutside'

const mapDispatchToProps = (dispatch) => bindActionCreators({ addNewCard }, dispatch)

export default
@connect(() => ({}), mapDispatchToProps)
class Control extends React.Component {
  /**
   * Add new card
   */
  static propTypes = {
    boardId: PropTypes.number,
    cardArrayLength: PropTypes.number,

    addNewCard: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      title: null,
    }
    this.saveBtnRef = React.createRef()
    this.addBtnRef = React.createRef()
    this.textareaRef = React.createRef()
  }

  handleEditingStart = () => {
    this.setState({ title: '' },
      () => {
        this.textareaRef.current.focus()
        this.saveBtnRef.current.scrollIntoView({ behavior: 'smooth' })
      })
  }

  handleChange = (event) => this.setState({ title: event.target.value })

  handleKeyPress = (event) => event.key === 'Enter' && this.handleSave()

  handleSave = () => this.props.addNewCard(this.props.boardId, this.state.title)

  /**
   * We don't technically need an action for pressing the save button since it is outside the card area anyway.
   */
  handleClickOutside = () => this.state.title.length ? this.handleSave() : this.setState({ title: null })

  componentDidUpdate (prevProps, prevState, snapshot) {
    /**
     * We are clearing the state after the new card is inserted in DOM, so we can properly scroll to the end.
     */
    if ((prevProps.cardArrayLength !== this.props.cardArrayLength) && prevState.title !== null) {
      this.setState({ title: null }, () =>
        this.addBtnRef.current.scrollIntoView({ behavior: 'smooth' }))
    }
  }

  render () {
    return (
      <>
        {this.state.title !== null &&
          <ClickOutside onClickOutside={this.handleClickOutside}>
            <div className='card-wrapper'>
              <div className='card'>
                <textarea
                  ref={this.textareaRef}
                  rows='4' value={this.state.title} onChange={this.handleChange} onKeyPress={this.handleKeyPress}
                />
              </div>
            </div>
          </ClickOutside>}
        {this.state.title !== null
          /**
           * Save action taken care of by clicking outside.
           */
          ? <div className='btn' ref={this.saveBtnRef}>Save</div>
          : <div className='btn' onClick={this.handleEditingStart} ref={this.addBtnRef}>Add new card</div>}
      </>
    )
  }
}
