const [USERS, USER, BOARDS, CARDS, TAGS] = [...new Array(5).keys()]
const mockData = {
  [USERS]: {
    1: {
      id: 1,
      firstName: 'Darius',
      lastName: 'Marian',
      email: 'darius_marian@blink.com',
    },
    2: {
      id: 2,
      firstName: 'Mihaita',
      lastName: 'Leoveanu',
      email: 'mihaita_leoveanu@blink.com',
    },
    3: {
      id: 3,
      firstName: 'Andrei',
      lastName: 'Grigorean',
      email: 'andrei_grigorean@blink.com',
    },
    4: {
      id: 4,
      firstName: 'Mihai',
      lastName: 'Ciucu',
      email: 'mihai_ciucu@blink.com',
    },
    5: {
      id: 5,
      firstName: 'Vlad',
      lastName: 'Tarniceru',
      email: 'vlad_tarniceru@blink.com',
    },
  },
  [USER]: null,
  [BOARDS]: {
    1: {
      id: 1,
      title: 'backlog',
      cards: [
        1,
        2,
        6,
        7
      ]
    },
    2: {
      id: 2,
      title: 'in progress',
      cards: [
        3,
        8
      ]
    },
    3: {
      id: 3,
      title: 'in review',
      cards: [
        5
      ]
    },
    4: {
      id: 4,
      title: 'done',
      cards: [
        4
      ]
    }
  },
  [CARDS]: {
    1: {
      id: 1,
      board: 1,
      title: '“A stockbroker urged me to buy a stock that would triple its value every year. I told him, ‘At my age, I don’t even buy green bananas.\'”\n',
      description: 'Kurt Vonnegut Jr. was an American writer. In a career spanning over 50 years, Vonnegut published fourteen novels, three short story collections, five plays, and five works of non-fiction, with further collections being published after his death.\n',
      tags: [
        2,
        3
      ],
      members: [
        1,
        2
      ]
    },
    2: {
      id: 2,
      board: 1,
      title: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      tags: [
        1,
        3
      ],
      members: []
    },
    3: {
      id: 3,
      board: 2,
      title: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      description: '',
      tags: [
        1,
        4,
        3,
        2
      ],
      members: []
    },
    4: {
      id: 4,
      board: 4,
      title: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
      description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
      tags: [
        3
      ],
      members: [
        1,
        4,
        5
      ]
    },
    5: {
      id: 5,
      board: 3,
      title: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
      description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
      tags: [
        2,
        3
      ],
      members: [
        3,
        2,
        1
      ]
    },
    6: {
      id: 6,
      board: 1,
      title: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
      description: '',
      tags: [],
      members: []
    },
    7: {
      id: 7,
      board: 1,
      title: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.',
      description: '',
      tags: [],
      members: []
    },
    8: {
      id: 8,
      board: 2,
      title: 'In order to print',
      description: '',
      tags: [
        1
      ],
      members: []
    }
  },
  [TAGS]: {
    1: {
      id: 1,
      text: 'bug',
    },
    2: {
      id: 2,
      text: 'frontend',
    },
    3: {
      id: 3,
      text: 'design',
    },
    4: {
      id: 4,
      text: 'backend',
    },
  }
}

export { USERS, USER, BOARDS, CARDS, TAGS }
export default mockData
