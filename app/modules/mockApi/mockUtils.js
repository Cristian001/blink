import mockData, { USERS, USER, BOARDS, TAGS, CARDS } from './mockData'

const LOCAL_STORAGE_PREFIX = 'blink_mock_data'

export function setInLocalStorage (collectionKey, value) {
  localStorage.setItem(`${LOCAL_STORAGE_PREFIX}_${collectionKey}`, JSON.stringify(value))
}

export function removeFromLocalStorage (collectionKey) {
  localStorage.removeItem(`${LOCAL_STORAGE_PREFIX}_${collectionKey}`)
}

export function getFromLocalStorage (collectionKey) {
  return JSON.parse(localStorage.getItem(`${LOCAL_STORAGE_PREFIX}_${collectionKey}`))
}

export function initializeLocalStorage () {
  for (const collectionKey of [USERS, USER, BOARDS, TAGS, CARDS]) {
    if (getFromLocalStorage(collectionKey) === null) {
      setInLocalStorage(collectionKey, mockData[collectionKey])
    }
  }
}

/**
 * This decorator is used to persist whatever changes in store to localStorage. As a mocked backend the localStorage is
 * being modified first and then the redux store, and since the localStorage is too nice to error, we can safely
 * do so. Of course, the application breaks if the localStorage is altered directly but I consider this beyond the
 * scope of the project.
 */
export function wrapReducer (collectionKey) {
  return function decorator (reducer) {
    return function (state, action) {
      if (action.type === 'LOCAL_STORAGE_UPDATED') {
        if (collectionKey === parseInt(action.key.match(/\d+/)[0])) {
          console.log(action.state)
          return JSON.parse(action.state)
        }
        return state
      } else {
        const result = reducer(state, action)
        setInLocalStorage(collectionKey, result)
        return result
      }
    }
  }
}
