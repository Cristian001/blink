import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { moveCard } from './actions'

import Control from './boardComponents/Control'
import Title from './boardComponents/Title'
import Card from './boardComponents/Card'

const mapStateToProps = (state) => ({
  boards: state.boards,
})
const mapDispatchToProps = (dispatch) => bindActionCreators({ moveCard }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Board extends React.Component {
  static propTypes = {
    id: PropTypes.number,
    boards: PropTypes.object,
    draggedNode: PropTypes.object,
    draggedNodeId: PropTypes.number,
    sourceBoardId: PropTypes.number,
    currentTargetBoardId: PropTypes.number,

    moveCard: PropTypes.func,
    setCurrentTargetBoardId: PropTypes.func,
    setup: PropTypes.func,
    reset: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      cardArray: props.boards[props.id].cards,
      isDraggedOver: false,
    }
    this.boardRef = React.createRef()
    this.wrapperRef = React.createRef()
    this.scrollerRef = React.createRef()
  }

  getBoard = (props) => props.boards[this.props.id]

  /**
   * Drag and drop
   */
  getWrapperVerticalBreakpoints = () => {
    const { top, bottom } = this.wrapperRef.current.getBoundingClientRect()
    return { top, bottom }
  }

  handleDragEnter = (event) => {
    if (this.props.currentTargetBoardId !== this.props.id) {
      this.props.setCurrentTargetBoardId(this.props.id)
    }
  }

  handleDragOver = (event) => {
    /**
     * Prevent dragover default so we can eventually drop
     */
    event.preventDefault()
    /**
     * If current dragged DOM node id is in the node array, we remove it before inserting it in the correct position
     */
    const cardArray = [...this.getBoard(this.props).cards]
    const draggedNodeIndex = cardArray.indexOf(this.props.draggedNodeId)
    if (draggedNodeIndex >= 0) {
      cardArray.splice(draggedNodeIndex, 1)
    }
    /**
     * Check if we are above or bellow all nodes. This is necessary when entering the drop zone without touching any
     * node and their respective checks are not triggered. Insert the dragged node id at the beginning or end of the
     * node array. After entering no additional check is needed so we stop.
     */
    const { top, bottom } = this.getWrapperVerticalBreakpoints()
    if (!this.state.isDraggedOver) {
      if (event.clientY <= top - this.scrollerRef.current.scrollTop) {
        return this.setState({ cardArray: [this.props.draggedNodeId, ...cardArray], isDraggedOver: true })
      }
      if (event.clientY >= bottom) {
        return this.setState({ cardArray: [...cardArray, this.props.draggedNodeId], isDraggedOver: true })
      }
    }
    /**
     * Node checks. This is where we check if we PASS above or bellow the current node midpoint and alter the node
     * array by inserting the dragged node id either before of after this current node id.
     */
    if (event.target.dataset.id && (parseInt(event.target.dataset.id) !== this.props.draggedNodeId)) {
      const { height, top } = event.target.getBoundingClientRect()
      const inNodeMouseY = event.clientY - top

      const targetCardId = parseInt(event.target.dataset.id)
      if (inNodeMouseY > height / 2) {
        cardArray.splice(cardArray.indexOf(targetCardId) + 1, 0, this.props.draggedNodeId)
      } else {
        cardArray.splice(cardArray.indexOf(targetCardId), 0, this.props.draggedNodeId)
      }
      return this.setState({ cardArray })
    }
  }

  handleDrop = () => {
    this.setState({ isDraggedOver: false })
    this.props.reset()
    this.props.moveCard(this.props.draggedNodeId, this.state.cardArray, this.props.sourceBoardId, this.props.id)
  }

  maybeRemoveCard = (prevProps) => {
    /**
     * If currently dragging and the card just entered a new board, different from this one, search for the card in the
     * card array and possibly remove it. We do this to ensure we only remove the card DOM node from the last board
     * it was on, only if it has found a new home.
     */
    if (this.props.draggedNode) {
      if (prevProps.currentTargetBoardId !== this.props.currentTargetBoardId &&
        this.props.currentTargetBoardId !== this.props.id) {
        const draggedNodeId = this.props.draggedNodeId
        const draggedNodeIndex = this.state.cardArray.indexOf(draggedNodeId)
        if (draggedNodeIndex > -1) {
          const cardArray = [...this.state.cardArray]
          cardArray.splice(draggedNodeIndex, 1)
          this.setState({ cardArray, isDraggedOver: false })
        }
      }
    }
  }

  maybeUpdateCardArray = (prevProps) => {
    /**
     * Update the card array if we receive a new card in props.
     */
    if (this.getBoard(prevProps).cards.length !== this.getBoard(this.props).cards.length) {
      this.setState({ cardArray: this.getBoard(this.props).cards })
    }
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    this.maybeRemoveCard(prevProps)
    this.maybeUpdateCardArray(prevProps)
  }

  render () {
    const board = this.getBoard(this.props)
    return (
      <div
        className='board' ref={this.boardRef} onDragEnter={this.handleDragEnter}
        onDragOver={this.handleDragOver} onDrop={this.handleDrop}
      >
        <Title boardId={this.props.id} />
        <div className='card-area' ref={this.scrollerRef}>
          <div className='cards-wrapper' ref={this.wrapperRef}>
            {this.state.cardArray.map((cardId) =>
              <Card id={cardId} setup={this.props.setup} key={cardId} isDragging={!!this.props.draggedNode} />
            )}
          </div>
          <Control boardId={this.props.id} cardArrayLength={board.cards.length} />
        </div>
      </div>
    )
  }
}
