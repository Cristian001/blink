import { take, select, put, fork, delay, all } from 'redux-saga/effects'
import { push } from 'connected-react-router'
import { LOGIN, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, CLEAR_ERROR, REDIRECT_TO_LOGIN } from './actions'

function * loginFlow () {
  /**
   * Sagas are simple generator functions which can yield anything. In order to interact with the saga middleware, the
   * API uses constructs called "effects", which are basically plain JS objects. Such effects are take, select, put.
   *
   * This saga implements the login flow. The "take" effect instructs the saga middleware to suspend this current saga
   * execution until a redux action of the type LOGIN is fired. The login action defined in actions.js has an email
   * key, which we unpack in email constant. The "select" effect is used to get all the mocked users in the redux store.
   *
   * We then try to find an user with email = email in all the mocked users.
   *
   * If we find an user, it means the login is successful, so we instruct the middleware using the "put" effect to
   * dispatch a LOGIN_SUCCESS action and also send the user object we found so the reducer can alter the store.
   * We are now waiting for the LOGOUT action. After the LOGOUT action is fired, the saga continues from the top,
   * waiting for LOGIN ...
   *
   * Otherwise, if an error occurs we dispatch a LOGIN_FAIL action and we skip the current iteration, waiting for LOGIN
   * again. "fork" effect receives another saga and executes it non-blocking, which means in this generator we continue
   * execution immediately. In clearError generator we block until "delay" yields and then we dispatch the clearError
   * action.
   */
  while (true) {
    const { email } = yield take(LOGIN)
    const allUsers = yield select((state) => state.users)
    const user = Object.values(allUsers).find((userObj) => userObj.email === email)
    if (user) {
      yield put({ type: LOGIN_SUCCESS, user })
      yield put(push('/home'))
    } else {
      yield put({ type: LOGIN_FAIL })
      yield fork(clearError)
      continue
    }
    yield take(LOGOUT)
  }
}

function * clearError () {
  yield delay(2000)
  yield put({ type: CLEAR_ERROR })
}

function * redirectToLogin () {
  /**
   * Here we lister to REDIRECT_TO_LOGIN action in order to change route to login. The utility of this saga is not
   * apparent if we just redirect to login, wait for successful login and redirect to home. This can be done directly
   * in components. But if we want to save the URL before login in order to redirect to it, we need not save it in
   * the store, and this reduces complexity.
   */
  while (true) {
    yield take([REDIRECT_TO_LOGIN, LOGOUT])
    // const pathName = yield select((state) => state.router.location.pathname)
    yield put(push('/'))
    yield take(LOGIN_SUCCESS)
    yield put(push('/home'))
    // yield put(push(pathName))
  }
}

export function * authSaga () {
  yield all([fork(loginFlow), fork(redirectToLogin)])
}
