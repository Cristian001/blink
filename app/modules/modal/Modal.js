import PropTypes from 'prop-types'
import React from 'react'
import ReactDOM from 'react-dom'

const modalRoot = document.getElementById('modal-root')

export default class Modal extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    canClose: PropTypes.bool,

    onClose: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      innerClicked: false,
    }
    this.el = document.createElement('div')
    this.overlayRef = React.createRef()
    this.innerRef = React.createRef()
  }

  canClose = () => this.props.canClose && !this.state.innerClicked

  close = () => this.canClose() && this.props.onClose()

  handleInnerKeyDown = (event) => {
    event.key === 'Escape' && this.close()
  }

  handleInnerMouseDown = () => {
    this.setState({ innerClicked: true })
  }

  handleInnerMouseUp = () => {
    this.setState({ innerClicked: false })
  }

  handleOverlayClick = (event) => {
    event.stopPropagation()
    if (event.target === this.overlayRef.current) {
      !this.state.innerClicked && this.close()
    }
  }

  componentDidMount () {
    modalRoot.appendChild(this.el)
    this.innerRef.current.focus()
  }

  componentWillUnmount () {
    modalRoot.removeChild(this.el)
  }

  renderModal = () => (
    <div className='overlay' onClick={this.handleOverlayClick} ref={this.overlayRef}>
      <div
        className='inner' onKeyDown={this.handleInnerKeyDown} tabIndex='0' ref={this.innerRef}
        onMouseDown={this.handleInnerMouseDown} onMouseUp={this.handleInnerMouseUp}
      >
        {this.props.children}
      </div>
    </div>
  )

  render () {
    return ReactDOM.createPortal(
      this.renderModal(),
      this.el,
    )
  }
}
