import PropTypes from 'prop-types'
import React from 'react'

import { CloseOutlined } from '@ant-design/icons'
import ClickOutside from './ClickOutside'

export default class Popup extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    title: PropTypes.string,
    visible: PropTypes.bool,
    right: PropTypes.number,

    onClose: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      right: null,
      top: null,
    }
    this.wrapperRef = React.createRef()
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    /**
     * Move the popup window to the left and up if it renders outside the viewport.
     */
    if (!prevProps.visible && this.props.visible) {
      const { right, bottom } = this.wrapperRef.current.getBoundingClientRect()
      if (right > window.innerWidth) {
        const offset = right - window.innerWidth
        const currentRight = parseInt(getComputedStyle(this.wrapperRef.current).right)
        this.setState({ right: currentRight + offset + 6 })
      }

      if (bottom > window.innerHeight) {
        const offset = bottom - window.innerHeight
        const currentTop = parseInt(getComputedStyle(this.wrapperRef.current).top)
        this.setState({ top: currentTop - offset - 6 })
      }
    }
    if (prevProps.visible && !this.props.visible) {
      this.setState({ right: null, top: null })
    }
  }

  render () {
    if (!this.props.visible) {
      return null
    }
    /**
     * We are passing dummy props _right or _top if we don't have to alter the positioning on either axis.
     */
    const style = {
      [`${this.state.right === null ? '_' : ''}right`]: this.state.right,
      [`${this.state.top === null ? '_' : ''}top`]: this.state.top,
    }
    return (
      <ClickOutside onClickOutside={this.props.onClose}>
        <div className='popup' ref={this.wrapperRef} style={style}>
          <div className='popup-title'>
            <h3>{this.props.title}</h3>
            <CloseOutlined onClick={() => this.props.onClose()} />
          </div>
          <div className='separator' />
          <div className='items'>
            {this.props.children}
          </div>
        </div>
      </ClickOutside>
    )
  }
}
