import invariant from 'invariant'
import isEmpty from 'lodash/isEmpty'
import isFunction from 'lodash/isFunction'
import isString from 'lodash/isString'

import checkStore from './checkStore'
import createReducer from '../reducers'

function injectReducerFactory (store) {
  return function (key, reducer) {
    invariant(
      isString(key) && !isEmpty(key) && isFunction(reducer),
      '(app/utils...) injectReducer: Expected `reducer` to be a reducer function'
    )

    if (key in store.injectedReducers && store.injectedReducers[key] === reducer) return

    store.injectedReducers[key] = reducer
    store.replaceReducer(createReducer(store.injectedReducers))
    store.dispatch({ type: '@@REDUCER_INJECTED' })
  }
}

// Multiple reducers at once
function injectReducersFactory (store) {
  return function (reducers) {
    for (const { key, reducer } of reducers) {
      invariant(
        isString(key) && !isEmpty(key) && isFunction(reducer),
        '(app/utils...) injectReducer: Expected `reducer` to be a reducer function'
      )

      if (key in store.injectedReducers && store.injectedReducers[key] === reducer) return

      store.injectedReducers[key] = reducer
    }
    store.replaceReducer(createReducer(store.injectedReducers))
    store.dispatch({ type: '@@REDUCERS_INJECTED' })
  }
}

export default function getInjectors (store) {
  checkStore(store)

  return {
    injectReducer: injectReducerFactory(store),
    injectReducers: injectReducersFactory(store),
  }
}
