/**
 * Actions are the way we alter the redux store, or interact with sagas. Actions are plain JS objects that must
 * have a type key. We can either dispatch them in react components or is sagas.
 */
export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const login = (email) => ({ type: LOGIN, email })

export const CLEAR_ERROR = 'CLEAR_ERROR'
export const LOGOUT = 'LOGOUT'
export const logout = () => ({ type: LOGOUT })

export const REDIRECT_TO_LOGIN = 'REDIRECT_TO_LOGIN'
export const redirectToLogin = () => ({ type: REDIRECT_TO_LOGIN })
