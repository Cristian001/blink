import { BOARDS, CARDS, TAGS } from '../mockApi/mockData'
import { getFromLocalStorage, wrapReducer } from '../mockApi/mockUtils'

import { CREATE_CARD, MOVE_CARD, CHANGE_BOARD_TITLE, MOVE_ALL_CARDS, UPDATE_CARD, CHANGE_SEARCH_TERM } from './actions'

const boardsActionMap = {
  [CREATE_CARD]: (state, action) => ({
    ...state,
    [action.boardId]: {
      ...state[action.boardId],
      cards: [...state[action.boardId].cards, action.cardId]
    }
  }),
  [MOVE_CARD]: (state, action) => {
    const sourceBoardCardArray = [...state[action.sourceBoardId].cards]
    sourceBoardCardArray.splice(sourceBoardCardArray.indexOf(action.cardId), 1)
    const targetBoardCardArray = [...action.cardArray]
    return {
      ...state,
      [action.sourceBoardId]: {
        ...state[action.sourceBoardId],
        cards: sourceBoardCardArray,
      },
      [action.targetBoardId]: {
        ...state[action.targetBoardId],
        cards: targetBoardCardArray,
      },
    }
  },
  [CHANGE_BOARD_TITLE]: (state, action) => ({
    ...state,
    [action.boardId]: {
      ...state[action.boardId],
      title: action.boardTitle,
    }
  }),
  [MOVE_ALL_CARDS]: (state, action) => ({
    ...state,
    [action.sourceBoardId]: {
      ...state[action.sourceBoardId],
      cards: [],
    },
    [action.targetBoardId]: {
      ...state[action.targetBoardId],
      cards: [...state[action.sourceBoardId].cards, ...state[action.targetBoardId].cards]
    }
  })
}

export const boardsReducer = wrapReducer(BOARDS)((state = getFromLocalStorage(BOARDS), action) =>
  boardsActionMap.hasOwnProperty(action.type) ? boardsActionMap[action.type](state, action) : state)

const cardsActionMap = {
  [CREATE_CARD]: (state, action) => ({
    ...state,
    [action.cardId]: {
      id: action.cardId,
      board: action.boardId,
      title: action.text,
      description: '',
      tags: [],
      members: [],
    }
  }),
  [MOVE_CARD]: (state, action) => ({
    ...state,
    [action.cardId]: {
      ...state[action.cardId],
      board: action.targetBoardId,
    }
  }),
  [MOVE_ALL_CARDS]: (state, action) => {
    const cardMap = {}
    Object.values(state)
      .forEach((card) => {
        if (card.board === action.sourceBoardId) {
          cardMap[card.id] = { ...state[card.id], board: action.targetBoardId }
        }
      })
    return { ...state, ...cardMap }
  },
  [UPDATE_CARD]: (state, action) => ({
    ...state,
    [action.cardId]: {
      ...state[action.cardId],
      [action.key]: action.value,
    }
  })
}

export const cardsReducer = wrapReducer(CARDS)((state = getFromLocalStorage(CARDS), action) =>
  cardsActionMap.hasOwnProperty(action.type) ? cardsActionMap[action.type](state, action) : state)

export const tagsReducer = (state = getFromLocalStorage(TAGS), action) => state

export const searchReducer = (state = '', action) => {
  if (action.type === CHANGE_SEARCH_TERM) {
    return action.searchTerm
  }
  return state
}
