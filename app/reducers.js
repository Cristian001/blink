
/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import history from 'utils/history'
import { usersReducer, userReducer } from './modules/auth/reducer'
import { boardsReducer, cardsReducer, tagsReducer, searchReducer } from './modules/home/reducer'

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer (injectedReducers = {}) {
  return combineReducers({
    user: userReducer,
    users: usersReducer,
    boards: boardsReducer,
    cards: cardsReducer,
    tags: tagsReducer,
    searchTerm: searchReducer,
    router: connectRouter(history),
    ...injectedReducers,
  })
}
