import PropTypes from 'prop-types'
import React, { Component } from 'react'

/**
 * Component that alerts if you click outside of it
 */
export default class OutsideAlerter extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),

    onClickOutside: PropTypes.func,
  }

  constructor (props) {
    super(props)

    this.wrapperRef = React.createRef()
  }

  componentDidMount () {
    document.addEventListener('click', this.handleClickOutside)
  }

  componentWillUnmount () {
    document.removeEventListener('click', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (this.wrapperRef.current && !this.wrapperRef.current.contains(event.target)) {
      this.props.onClickOutside()
    }
  }

  render () {
    return <div className='ignore' ref={this.wrapperRef}>{this.props.children}</div>
  }
}
