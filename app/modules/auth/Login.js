import { CloseOutlined } from '@ant-design/icons'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { bindActionCreators } from 'redux'

import logo from '../../images/logo.svg'
import { login } from './actions'

const mapStateToProps = (state) => ({ user: state.user })
const mapDispatchToProps = (dispatch) => bindActionCreators({ login }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Login extends React.Component {
  static propTypes = {
    user: PropTypes.object,

    login: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      invalidEmail: false,
      email: '',
      password: '',
    }
  }

  checkEmail = () => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(this.state.email).toLowerCase())
  }

  attemptLogin = (email) => !this.checkEmail()
    ? this.setState({ invalidEmail: true }, this.clearError)
    : this.props.login(email)

  clearError = () => setTimeout(() => this.setState({ invalidEmail: false }), 2000)

  handleChange = (key, value) => this.setState({ [key]: value })

  handleClick = () => this.attemptLogin(this.state.email)

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.attemptLogin(this.state.email)
    }
  }

  render () {
    if (this.props.user !== null && this.props.user.hasOwnProperty('email')) {
      return <Redirect to='/home' />
    }
    const hasError = this.state.invalidEmail ||
      (this.props.user !== null && this.props.user.hasOwnProperty('error'))
    return (
      <div className='login'>
        <img className='logo' src={logo} alt='Blink' />
        <div className='form' onKeyPress={this.handleKeyPress}>
          <h1 className='title'>
            Log In
          </h1>
          <div className={'input-wrapper' + (hasError ? ' error' : '')}>
            <input
              type='email' placeholder='Enter email' value={this.state.email}
              onChange={(event) => this.handleChange('email', event.target.value)}
            />
            {hasError && <div className='feedback'><CloseOutlined /></div>}
          </div>
          <div className={'input-wrapper' + (hasError ? ' error' : '')}>
            <input
              type='password' placeholder='Enter password' value={this.state.password}
              onChange={(event) => this.handleChange('password', event.target.value)}
            />
            {hasError && <div className='feedback'><CloseOutlined /></div>}
          </div>
          {hasError &&
            <div className='error-display'>
              {this.state.invalidEmail ? 'Invalid email format' : this.props.user.error}
            </div>}
          <div className='btn' onClick={this.handleClick}>Log In</div>
        </div>
      </div>
    )
  }
}
