import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'

import Modal from '../modal/Modal'
import { updateCard } from '../home/actions'
import TextArea from '../reusable/TextArea'
import Popup from '../reusable/Popup'
import Header from '../home/Header'

const mapStateToProps = (state) => ({
  cards: state.cards,
  tags: state.tags,
  users: state.users,
})
const mapDispatchToProps = (dispatch) => bindActionCreators({ updateCard }, dispatch)

export default @connect(mapStateToProps, mapDispatchToProps)
@withRouter
class CardModal extends React.Component {
  static propTypes = {
    match: PropTypes.object,
    history: PropTypes.object,
    tags: PropTypes.object,
    cards: PropTypes.object,
    users: PropTypes.object,

    updateCard: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      isChoosingTags: false,
      isChoosingMembers: false,
    }
    this.titleRef = React.createRef()
  }

  getCard = () => this.props.cards[this.props.match.params.id]

  handleModalClose = () => this.props.history.push('/home')

  handleTitleSubmit = (text) => this.props.updateCard(this.getCard().id, 'title', text)

  handleDescriptionSubmit = (text) => this.props.updateCard(this.getCard().id, 'description', text)

  handleToggleTagPopup = () => this.setState({ isChoosingTags: !this.state.isChoosingTags })

  handleToggleMembersPopup = () => this.setState({ isChoosingMembers: !this.state.isChoosingMembers })

  handleTagClick = (tagId) => {
    const cardTags = [...this.getCard().tags]
    const index = cardTags.indexOf(tagId)
    index > -1 ? cardTags.splice(index, 1) : cardTags.push(tagId)
    return this.props.updateCard(this.getCard().id, 'tags', cardTags)
  }

  handleMemberClick = (userId) => {
    const cardMembers = [...this.getCard().members]
    const index = cardMembers.indexOf(userId)
    index > -1 ? cardMembers.splice(index, 1) : cardMembers.push(userId)
    return this.props.updateCard(this.getCard().id, 'members', cardMembers)
  }

  render () {
    const card = this.getCard()
    return (
      <Modal onClose={this.handleModalClose} canClose={!(this.state.isChoosingTags || this.state.isChoosingMembers)}>
        <CloseOutlined onClick={this.handleModalClose} className='large'/>
        <div className='title' ref={this.titleRef}>
          <TextArea
            canTrigger={!(this.state.isChoosingTags || this.state.isChoosingMembers)}
            offsetHeight={26} placeholder='Add title...' text={card.title}
            onSubmit={this.handleTitleSubmit}
          />
        </div>
        <div className='add-label'>
          <h4>Labels</h4>
          <div className='btn' onClick={this.handleToggleTagPopup}>Add label</div>
          <Popup title='LABELS' onClose={this.handleToggleTagPopup} visible={this.state.isChoosingTags}>
            <div className='tags'>
              {Object.values(this.props.tags).map((tag) =>
                <div className={`tag ${tag.text}`} key={tag.id} onClick={() => this.handleTagClick(tag.id)}>
                  <span>{tag.text}</span>
                  {card.tags.includes(tag.id) && <CheckOutlined />}
                </div>)}
            </div>
          </Popup>
        </div>
        <div className='tags'>
          {card.tags.map((tagId) => {
            const tagText = this.props.tags[tagId].text
            return <div className={`tag ${tagText}`} key={tagId}>{tagText}</div>
          })}
        </div>
        <h4>Description</h4>
        <div className='description'>
          <TextArea
            canTrigger={!(this.state.isChoosingTags || this.state.isChoosingMembers)}
            offsetHeight={22} placeholder='Add description...' text={card.description}
            onSubmit={this.handleDescriptionSubmit}
          />
        </div>
        <div className='members'>
          <h4>Assigned members</h4>
          <div className='btn' onClick={this.handleToggleMembersPopup}>Add member</div>
          <Popup title='MEMBERS' onClose={this.handleToggleMembersPopup} visible={this.state.isChoosingMembers}>
            {Object.values(this.props.users).map((user) =>
              <div className='member-wrapper' key={user.id} onClick={() => this.handleMemberClick(user.id)}>
                <div className='member'>
                  <div className='caption'>{Header.getNameInitials(user.firstName, user.lastName)}</div>
                </div>
                <div>{user.firstName} {user.lastName}</div>
                {card.members.includes(user.id) && <CheckOutlined />}
              </div>)}
          </Popup>
        </div>
        <div className='member-list'>
          <div className='members'>
            {card.members.map((userId) => {
              const user = this.props.users[userId]
              return (
                <div className='member' key={user.id}>
                  <div className='caption'>{Header.getNameInitials(user.firstName, user.lastName)}</div>
                </div>)
            })}
          </div>
        </div>
        {/**
         * From here on out everything is static
         */}
        <h4>COMMENTS</h4>
        <div className='comments'>
          <div className='placeholder'>
            <div className='member'>
              <div className='caption'>VT</div>
            </div>
            <div className='input-wrapper'><input type='text' placeholder='Write a comment...' /></div>
          </div>
          <div className='comment'>
            <div className='header'>
              <div className='member'>
                <div className='caption'>ML</div>
              </div>
              <div className='presence'>
                <span className='name'>Mihaita Leoveanu</span>
                <span className='date'>on Feb 29, 2020 at 6:11 PM</span>
              </div>
            </div>
            <div className='input-wrapper'>
              <div>Caterinca.</div>
            </div>
          </div>
          <div className='comment'>
            <div className='header'>
              <div className='member'>
                <div className='caption'>DM</div>
              </div>
              <div className='presence'>
                <span className='name'>Darius Marian</span>
                <span className='date'>on Feb 28, 2020 at 3:00 PM</span>
              </div>
            </div>
            <div className='input-wrapper'>
              <div>What has Kurt Vonnegut to do with stockbrokers?</div>
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}
