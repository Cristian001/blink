import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { redirectToLogin } from './actions'

const mapStateToProps = (state) => ({ user: state.user })
const mapDispatchToProps = (dispatch) => bindActionCreators({ redirectToLogin }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Authenticated extends React.Component {
  /**
 * connect(mapDispatchToProps, mapDispatchToProps) decorator is the idiom used so the react component receives some
 * nodes of the store and actions wrapped in a dispatcher function so it can alter the store.
 */
  static propTypes = {
    user: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),

    redirectToLogin: PropTypes.func,
  }

  isAuthenticated = () => this.props.user && this.props.user.hasOwnProperty('email')

  componentDidMount () {
    if (!this.isAuthenticated()) {
      this.props.redirectToLogin()
    }
  }

  render () {
    if (this.isAuthenticated()) {
      return this.props.children
    } else {
      return null
    }
  }
}
