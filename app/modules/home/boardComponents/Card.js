import PropTypes from 'prop-types'
import React from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Tag from './Tag'

const mapStateToProps = (state) => ({
  cards: state.cards,
  tags: state.tags,
  searchTerm: state.searchTerm,
})
const mapDispatchToProps = (dispatch) => bindActionCreators({ }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
@withRouter
class Card extends React.Component {
  static propTypes = {
    id: PropTypes.number,
    history: PropTypes.object,
    cards: PropTypes.object,
    tags: PropTypes.object,
    searchTerm: PropTypes.string,
    isDragging: PropTypes.bool,

    setup: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.state = {
      minimized: false,
    }
    this.cardRef = React.createRef()
  }

  getCard = () => this.props.cards[this.props.id]

  handleDragStart = (event) => {
    event.dataTransfer.dropEffect = 'move'
    event.dataTransfer.setDragImage(this.cardRef.current, 128, 32)
    const card = this.getCard()
    this.props.setup(event.target, card.id, card.board, card.board)
  }

  handleCardClick = () => this.props.history.push(`/card/${this.getCard().id}`)

  toggleMinimize = () => this.setState({ minimized: !this.state.minimized })

  renderCard = () => {
    const card = this.getCard()
    /**
     * Boards update may happen before cards update when syncing the store across windows so we check against it here.
     */
    if (card === undefined) {
      return null
    }
    return (
      <div
        className={'card-wrapper' + (this.props.isDragging ? ' drag' : '')} key={card.id}
        data-id={card.id} draggable onDragStart={this.handleDragStart}
      >
        <div className='card' onClick={this.handleCardClick} ref={this.cardRef}>
          <div className='tags'>
            {card.tags.map((tagId) => {
              const tagText = this.props.tags[tagId].text
              return (
                <Tag
                  className={`tag hover ${tagText}`} key={tagId} text={tagText} minimized={this.state.minimized}
                  toggleMinimize={this.toggleMinimize}
                />)
            })}
          </div>
          <div className='headline'>{card.title}</div>
        </div>
      </div>
    )
  }

  render () {
    const card = this.getCard()
    if (this.props.searchTerm.length >= 3) {
      if (card.title.toLowerCase().includes(this.props.searchTerm.toLowerCase())) {
        return this.renderCard()
      }
      if (card.description.toLowerCase().includes(this.props.searchTerm.toLowerCase())) {
        return this.renderCard()
      }
      return null
    }
    return this.renderCard()
  }
}
