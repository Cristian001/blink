import React from 'react'
import { Route, Switch } from 'react-router-dom'

import injectSaga from '../../utils/injectSaga'
import rootSaga from '../../sagas'
import Authenticated from '../auth'
import Login from '../auth/Login'
import Home from '../home'

export default
@injectSaga({ key: 'root', saga: rootSaga })
class Main extends React.Component {
  render () {
    return (
      <Switch>
        <Route exact path='/'><Login /></Route>
        <Authenticated>
          <Route path='/:pattern'><Home /></Route>
        </Authenticated>
      </Switch>
    )
  }
}
