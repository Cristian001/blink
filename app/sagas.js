import { all, fork } from 'redux-saga/effects'

import { authSaga } from './modules/auth/saga'
import { cardActionsSaga } from './modules/home/sagas'

export default function * rootSaga () {
  yield all([fork(authSaga), fork(cardActionsSaga)])
}
