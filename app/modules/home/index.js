import React from 'react'
import { Route } from 'react-router-dom'
import Dashboard from './Dashboard'
import Header from './Header'
import CardModal from '../card'

export default
class Home extends React.Component {
  render () {
    return (
      <div className='home'>
        <Header />
        <Dashboard />
        <Route path='/card/:id'><CardModal /></Route>
      </div>
    )
  }
}
