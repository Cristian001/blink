export const CREATE_CARD = 'CREATE_CARD'

export const ADD_NEW_CARD = 'ADD_NEW_CARD'
export const addNewCard = (boardId, text) => ({ type: ADD_NEW_CARD, boardId, text })

export const MOVE_CARD = 'MOVE_CARD'
export const moveCard = (cardId, cardArray, sourceBoardId, targetBoardId) =>
  ({ type: MOVE_CARD, cardId, cardArray, sourceBoardId, targetBoardId })

export const CHANGE_BOARD_TITLE = 'CHANGE_BOARD_TITLE'
export const changeBoardTitle = (boardId, boardTitle) => ({ type: CHANGE_BOARD_TITLE, boardId, boardTitle })

export const MOVE_ALL_CARDS = 'MOVE_ALL_CARDS'
export const moveAllCards = (sourceBoardId, targetBoardId) => ({ type: MOVE_ALL_CARDS, sourceBoardId, targetBoardId })

export const UPDATE_CARD = 'UPDATE_CARD'
export const updateCard = (cardId, key, value) => ({ type: UPDATE_CARD, cardId, key, value })

export const CHANGE_SEARCH_TERM = 'CHANGE_SEARCH_TERM'
export const changeSearchTerm = (searchTerm) => ({ type: CHANGE_SEARCH_TERM, searchTerm })
