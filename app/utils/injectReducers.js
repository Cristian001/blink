import React from 'react'
import hoistNonReactStatics from 'hoist-non-react-statics'
import { ReactReduxContext } from 'react-redux'

import getInjectors from './reducerInjectors'

/**
 * Dynamically injects a reducer
 *
 * @param {Object[]} reducers A list of reducers
 * @param {string} reducers[].key
 * @param {function} reducers[].reducer
 *
 */
export default (reducers) => (WrappedComponent) => {
  class ReducersInjector extends React.Component {
    static WrappedComponent = WrappedComponent;

    static contextType = ReactReduxContext;

    static displayName = `withReducers(${(WrappedComponent.displayName || WrappedComponent.name || 'Component')})`;

    constructor (props, context) {
      super(props, context)

      getInjectors(context.store).injectReducers(reducers)
    }

    render () {
      return <WrappedComponent {...this.props} />
    }
  }

  return hoistNonReactStatics(ReducersInjector, WrappedComponent)
}
