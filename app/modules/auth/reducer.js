import { USERS, USER } from '../mockApi/mockData'
import { getFromLocalStorage, wrapReducer } from '../mockApi/mockUtils'

import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, CLEAR_ERROR } from './actions'

/**
 * The reducers are function which are called whenever an action is dispatched, after the middleware chain, regardless
 * if the said action has an associated handler in the reducer.
 * The users reducer just loads data from local storage in the store and basically does nothing.
 * The user reducer keeps track of authentication status.
 */
export const usersReducer = (state = getFromLocalStorage(USERS), action) => state

const userActionMap = {
  [LOGIN_SUCCESS]: (state, action) => action.user,
  [LOGIN_FAIL]: (state, action) => ({ error: 'Invalid credentials' }),
  [LOGOUT]: (state, action) => null,
  [CLEAR_ERROR]: (state, action) => null,
}

export const userReducer = wrapReducer(USER)((state = getFromLocalStorage(USER), action) =>
  userActionMap.hasOwnProperty(action.type) ? userActionMap[action.type](state, action) : state)
