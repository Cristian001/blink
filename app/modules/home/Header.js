import { SearchOutlined } from '@ant-design/icons'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { logout } from '../auth/actions'
import { changeSearchTerm } from './actions'

const mapStateToProps = (state) => ({ users: state.users, user: state.user, searchTerm: state.searchTerm })
const mapDispatchToProps = (dispatch) => bindActionCreators({ logout, changeSearchTerm }, dispatch)

export default
@connect(mapStateToProps, mapDispatchToProps)
class Header extends React.Component {
  static propTypes = {
    users: PropTypes.objectOf(PropTypes.object),
    user: PropTypes.object,
    searchTerm: PropTypes.string,

    logout: PropTypes.func,
    changeSearchTerm: PropTypes.func,
  }

  static getNameInitials = (firstName, lastName) => firstName.charAt(0).toUpperCase() + lastName.charAt(0).toUpperCase()

  constructor (props) {
    super(props)
    this.inputRef = React.createRef()
  }

  handleSearchClick = () => this.inputRef.current.focus()

  render () {
    return (
      <header className='green-bg'>
        <div className='members'>
          {Object.values(this.props.users).map((user, idx) =>
            <div className='member' key={idx}>
              <span className='caption'>
                {Header.getNameInitials(user.firstName, user.lastName)}
              </span>
            </div>,
          )}
        </div>
        <div className='search-bar' onClick={this.handleSearchClick}>
          <SearchOutlined />
          <div className='input-wrapper'>
            <input
              type='text' placeholder='Search card' value={this.props.searchTerm} ref={this.inputRef}
              onChange={(event) => this.props.changeSearchTerm(event.target.value)}
            />
          </div>
        </div>
        <div className='logged-in-user'>
            Logged in as <span>{`${this.props.user.firstName} ${this.props.user.lastName}`}</span>
        </div>
        <div className='logout' onClick={() => this.props.logout()}>Logout</div>
      </header>
    )
  }
}
