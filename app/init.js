import FontFaceObserver from 'fontfaceobserver'
import { initializeLocalStorage } from './modules/mockApi/mockUtils'

initializeLocalStorage()

const openSansObserver = new FontFaceObserver('Open Sans', {})

const registerOpenSans = () => openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded')
}, () => {
  document.body.classList.remove('fontLoaded')
})

export { registerOpenSans }
